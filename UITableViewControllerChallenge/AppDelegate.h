//
//  AppDelegate.h
//  UITableViewControllerChallenge
//
//  Created by RJ Militante on 1/23/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

